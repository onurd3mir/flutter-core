import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter_core/core/enums/service_path_enum.dart';
import 'package:flutter_core/core/manager/network_manager.dart';
import 'package:flutter_core/model/todo.dart';
import 'package:flutter_core/service/ITodoService.dart';

class TodoService extends ITodoService {
  @override
  Dio get dio => NetworkManager.instance.dio;

  @override
  Future<List<Todo>> getTodos() async {
    final String url =
        '${ServicePathEnum.BASE_URL.getUrl}${ServicePathEnum.TODO.getUrl}';
    final response = await dio.get(url);
    if (response.statusCode == HttpStatus.ok) {
      final data = response.data;
      if (data is List) return data.map((e) => Todo.fromJson(e)).toList();
    }
    return [];
  }
}
