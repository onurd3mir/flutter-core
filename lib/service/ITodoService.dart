import 'package:flutter_core/model/todo.dart';
import 'package:flutter_core/service/IAppService.dart';

abstract class ITodoService implements IAppService {
  Future<List<Todo>> getTodos();
}

