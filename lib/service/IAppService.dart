import 'package:dio/dio.dart';

abstract class IAppService {
  final Dio dio;
  IAppService(this.dio);
}
