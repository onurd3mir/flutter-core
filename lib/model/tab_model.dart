import 'package:flutter/cupertino.dart';

class TabModel {
  final String title;
  final IconData icon;
  final Widget page;

  TabModel({required this.title, required this.icon, required this.page});
}
