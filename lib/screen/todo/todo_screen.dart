import 'package:flutter/material.dart';
import 'package:flutter_core/core/base/base_state_mixin.dart';
import 'package:flutter_core/core/base/base_stateless.dart';
import 'package:flutter_core/core/manager/theme_manager.dart';
import 'package:flutter_core/screen/drawer/app_drawer.dart';
import 'package:flutter_core/store/todo_store.dart';
import 'package:flutter_core/widget/todo_card.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';


// ignore: must_be_immutable
class TodoScreen extends BaseStateless with BaseStateMixin {

  @override
  Widget build(BuildContext context) {
    this.buildContext = context;
    final TodoStore _todoStore = Get.put(TodoStore());

    return Scaffold(
      appBar: buildAppBar(context),
      body: Obx(
        () {
          if (_todoStore.isLoading) {
            return CircularProgressIndicator();
          } else {
            return GridView.builder(
              gridDelegate:
                  SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
              itemCount: _todoStore.todos.length,
              itemBuilder: (context, index) {
                return TodoCard(model: _todoStore.todos[index]);
              },
            );
          }
        },
      ),
        drawer: AppDrawer()
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      //leading: IconButton(icon: Icon(Icons.menu), onPressed: () {}),
      title: Text(appConstants.title,style: textTheme.headline6),
      centerTitle: true,
      actions: [
        IconButton(
          icon: context.read<ThemeManager>().isDarkMode
              ? Icon(Icons.light_mode)
              : Icon(Icons.dark_mode),
          onPressed: () {
            print( context.read<ThemeManager>().isDarkMode);
            context.read<ThemeManager>().changeTheme();
          },
        )
      ],
    );
  }
}
