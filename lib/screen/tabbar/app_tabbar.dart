import 'package:flutter/material.dart';
import 'package:flutter_core/model/tab_model.dart';
import 'package:flutter_core/screen/todo/todo_screen.dart';

class AppTabbar extends StatelessWidget {
  
  final List<TabModel> tabsItem = [
    TabModel(icon: Icons.business_center_outlined, page: TodoScreen(), title: 'Todos'),
    TabModel(icon: Icons.access_time,page:Container(color: Colors.purple[300],) ,title: 'Container')
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: tabsItem.length,
        child: Scaffold(
          bottomNavigationBar: BottomAppBar(child: buildTabBar()),
          body: buildTabBarView(),
        ));
  }

  TabBar buildTabBar() {
    return TabBar(
      tabs: List.generate(
        tabsItem.length,
        (index) => Tab(
          text: tabsItem[index].title,
          icon: Icon(tabsItem[index].icon),
        ),
      ),
    );
  }

  TabBarView buildTabBarView() =>
      TabBarView(children: tabsItem.map((e) => e.page).toList());
}