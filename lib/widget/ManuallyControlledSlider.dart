import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core/core/base/base_stateful.dart';

class ManuallyControlledSlider extends StatefulWidget {
  final List<dynamic> articles;

  const ManuallyControlledSlider({Key? key, required this.articles})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ManuallyControlledSliderState();
  }
}

class _ManuallyControlledSliderState
    extends BaseStateful<ManuallyControlledSlider> {
  final CarouselController _controller = CarouselController();

  _showDetailScreen(article) {
    // Navigator.push(context, MaterialPageRoute(builder: (_) {
    //   return DetailScreen(article: article);
    // }));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CarouselSlider(
          items: widget.articles
              .map((item) => GestureDetector(
                    onTap: () {
                      _showDetailScreen(item);
                    },
                    child: Container(
                      child: Container(
                        //margin: EdgeInsets.all(5.0),
                        child: ClipRRect(
                            //borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            child: Stack(
                          children: <Widget>[
                            Image.network(
                              item.urlToImage ?? imageContant.notFound,
                              fit: BoxFit.cover,
                              width: dynamicWidth(1),
                            ),
                            Positioned(
                              bottom: 0.0,
                              left: 0.0,
                              right: 0.0,
                              child: Container(
                                decoration: BoxDecoration(
                                  gradient: LinearGradient(
                                    colors: [
                                      Color.fromARGB(230, 0, 0, 0),
                                      Color.fromARGB(0, 0, 0, 0)
                                    ],
                                    begin: Alignment.bottomCenter,
                                    end: Alignment.topCenter,
                                  ),
                                ),
                                padding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                child: Text(titleEdit(item.title ?? ''),
                                    style: textTheme.bodyText1),
                              ),
                            ),
                          ],
                        )),
                      ),
                    ),
                  ))
              .toList(),
          options: CarouselOptions(
            enlargeCenterPage: true,
            height: dynamicHeight(0.25),
            autoPlay: true,
            viewportFraction: 1,
            //aspectRatio: MediaQuery.of(context).size.aspectRatio,
          ),
          carouselController: _controller,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            // Flexible(
            //   child: RaisedButton(
            //     onPressed: () => _controller.previousPage(),
            //     child: Text('←'),
            //   ),
            // ),
            // Flexible(
            //   child: RaisedButton(
            //     onPressed: () => _controller.nextPage(),
            //     child: Text('→'),
            //   ),
            // ),
            ...Iterable<int>.generate(widget.articles.length).map(
              (int pageIndex) => Flexible(
                child: ElevatedButton(
                  onPressed: () => _controller.animateToPage(pageIndex),
                  child: Text(
                    "${pageIndex + 1}",
                    style: textTheme.button!.copyWith(color: theme.buttonColor),
                  ),
                ),
              ),
            ),
          ],
        )
      ],
    );
  }
}

String titleEdit(String title) {
  if (title.contains('-')) {
    var result = title.split('-');
    var titleLen = title.length - (result[result.length - 1].length + 2);
    return title.substring(0, titleLen);
  } else {
    return title;
  }
}
