import 'package:flutter/material.dart';
import 'package:flutter_core/model/todo.dart';
import 'package:get/get.dart';

class TodoCard extends StatelessWidget {

  final Todo model;

  const TodoCard({Key? key, required this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: model.completed! ? Colors.green : Colors.red,
      child: ListTile(
        title: Text(model.userId.toString()),
        subtitle: Text(model.title ?? ''),
        onTap: () {
          if (model.completed!) {
            Get.snackbar('İşlem', 'Başarılı',
                snackPosition: SnackPosition.BOTTOM,
                backgroundColor: Colors.green);
          } else {
            Get.snackbar('İşlem', 'Başarısız', backgroundColor: Colors.red);
          }
        },
      ),
    );
  }
}
