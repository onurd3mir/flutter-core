import 'package:flutter/material.dart';
import 'package:flutter_core/core/base/base_state_mixin.dart';

// ignore: must_be_immutable
class ServiceError extends StatelessWidget with BaseStateMixin {
  @override
  Widget build(BuildContext context) {
    this.buildContext = context;
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.error_rounded, size: 100),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(appConstants.serviceError, style: theme.textTheme.headline6),
            ),
          ],
        ),
      ),
    );
  }
}
