import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_core/core/constants/image_constant.dart';

// ignore: must_be_immutable
class CacheNetworkImage extends StatelessWidget  {
  final String? imageUrl;

  const CacheNetworkImage({Key? key,required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  
    return CachedNetworkImage(
        imageUrl: imageUrl ?? ImageContant.instance.notFound,
        progressIndicatorBuilder: (context, url, downloadProgress) =>
            CircularProgressIndicator(
                value: downloadProgress.progress,
                color: Theme.of(context).primaryColorLight),
        errorWidget: (context, url, error) =>
            Image.network(ImageContant.instance.notFound));
  }
}
