import 'package:flutter/material.dart';
import 'package:flutter_core/core/constants/app_constant.dart';
import 'package:flutter_core/core/manager/locale_manager.dart';
import 'package:flutter_core/screen/tabbar/app_tabbar.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'core/constants/lang.dart';
import 'core/manager/theme_manager.dart';
import 'package:provider/provider.dart';

void main() async{
  await _init();
  return runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeManager>(
            create: (contex) => ThemeManager()),
      ],
      child: MyApp(),
    ),
  );
}

Future<void> _init() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocaleManager.prefrencesInit();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
       //locale: Get.deviceLocale,
      translations: Lang(), // your translations
      locale: Locale('tr', 'TR'),
      title: AppConstant.instance.title,
      theme: context.watch<ThemeManager>().currentData,
      home: AppTabbar(),
    );
  }
}
