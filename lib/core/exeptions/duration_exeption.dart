class DurationExeption implements Exception {
  final dynamic data;
  DurationExeption(this.data);

  @override
  String toString() {
    return 'Duration not found $data';
  }
}
