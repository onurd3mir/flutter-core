enum ServicePathEnum { BASE_URL, TODO }

extension ServicePathEnumExtensions on ServicePathEnum {
  String get getUrl {
    switch (this) {
      case ServicePathEnum.BASE_URL:
        return 'https://jsonplaceholder.typicode.com';
      case ServicePathEnum.TODO:
        return '/todos';
      default:
        throw Exception('not found uri');
    }
  }
}
