import 'package:flutter_core/core/exeptions/duration_exeption.dart';

enum DurationEnum { LOW, HIGHT, NORMAL }

extension DurationEnumsExtensions on DurationEnum {
    Duration get time {
    switch (this) {
      case DurationEnum.HIGHT:
        return Duration(seconds: 10);
      case DurationEnum.NORMAL:
        return Duration(seconds: 5);
      case DurationEnum.LOW:
        return Duration(seconds: 2);
      default:
       throw DurationExeption(this);
    }
  }
}