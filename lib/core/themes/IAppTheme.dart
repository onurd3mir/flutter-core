import 'package:flutter/material.dart';
import 'package:flutter_core/core/color/color_theme.dart';

abstract class IAppTheme {
  ThemeData get data;
  final colors = ColorTheme();
}

