import 'package:flutter/material.dart';
import 'package:flutter_core/core/constants/app_constant.dart';
import 'package:flutter_core/core/constants/image_constant.dart';

abstract class BaseStateful<T extends StatefulWidget> extends State<T> {

  AppConstant get appConstants => AppConstant.instance;
  ImageContant get imageContant => ImageContant.instance;
  TextTheme get textTheme => Theme.of(context).textTheme;
  ThemeData get theme => Theme.of(context);
  ColorScheme get colorScheme => Theme.of(context).colorScheme;

  double dynamicHeight(double value) => MediaQuery.of(context).size.height * value;
  double dynamicWidth(double value) => MediaQuery.of(context).size.width * value;

}
