import 'package:flutter/material.dart';
import 'package:flutter_core/core/constants/app_constant.dart';
import 'package:flutter_core/core/constants/image_constant.dart';

mixin BaseStateMixin {

  late BuildContext buildContext;

  AppConstant get appConstants => AppConstant.instance;
  ImageContant get imageContant => ImageContant.instance;

  TextTheme get textTheme => Theme.of(buildContext).textTheme;
  ThemeData get theme => Theme.of(buildContext);
  ColorScheme get colorScheme => Theme.of(buildContext).colorScheme;

  double dynamicHeight(double value) => MediaQuery.of(buildContext).size.height * value;
  double dynamicWidth(double value) => MediaQuery.of(buildContext).size.width * value;

}