class ImageContant {
  static ImageContant? _instance;
  static ImageContant get instance {
    if (_instance != null) return _instance!;
    _instance = ImageContant._init();
    return _instance!;
  }

    ImageContant._init();
    final String randomImage = 'https://picsum.photos/200/300';
     final String notFound = 'https://www.natro.com/hosting-sozlugu/wp-content/uploads/2015/12/404-not-found.png';
    final apparel = 'Apparel'.toPng; //example
   
}

extension _ImageExtensions on String {
    String get toPng => 'assets/image/$this.png';
    String get toJpg => 'assets/image/$this.jpg';
}

