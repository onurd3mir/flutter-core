import 'package:flutter/material.dart';
import 'package:flutter_core/core/enums/local_key_enum.dart';
import 'package:flutter_core/core/manager/locale_manager.dart';
import 'package:flutter_core/core/themes/app_dark_theme.dart';
import 'package:flutter_core/core/themes/app_light_theme.dart';

class ThemeManager extends ChangeNotifier {

  LocaleManager _localeManager = LocaleManager.instance;
  ThemeData currentData = AppLightTheme.instance.data;
  bool _isDarkMode = false;

  ThemeManager(){
    this.loadThme();
  }

  bool get isDarkMode => _isDarkMode;
  
  loadThme(){
    if(_localeManager.getBoolValue(PreferencesKeys.DARKMODE)){
      currentData = AppDarkTheme.instance.data;
      _isDarkMode = true;
    }else{
      currentData = AppLightTheme.instance.data;
      _isDarkMode = false;
    }
  }

  Future<void> changeTheme() async {
    if (!_isDarkMode) {
      currentData = AppDarkTheme.instance.data;
      _isDarkMode = true;
      await _localeManager.setBoolValue(PreferencesKeys.DARKMODE, true);
     
    } else {
      currentData = AppLightTheme.instance.data;
      _isDarkMode = false;
      await _localeManager.setBoolValue(PreferencesKeys.DARKMODE, false);
    }
    notifyListeners();
  }
}
