import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class NetworkManager {
  static NetworkManager? _instance;

  static NetworkManager get instance {
    if (_instance != null) return _instance!;
    _instance = NetworkManager._init();
    return instance;
  }

  // burada shared prefence dan token alıp dio interceptor ile gönderebiliriz.

  late final Dio dio;

  NetworkManager._init() {
    dio = Dio(BaseOptions());
    //if (kDebugMode) dio.interceptors.add(PrettyDioLogger());
    // dio.interceptors.add(InterceptorsWrapper(
    //   onRequest: (options, handler) =>
    //       {options.headers["Authorization"] = "Bearer ", handler.next(options)},
    // ));
  }
}
