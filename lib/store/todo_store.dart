import 'package:flutter_core/service/ITodoService.dart';
import 'package:flutter_core/service/todo_service.dart';
import 'package:get/get.dart';

class TodoStore extends GetxController {
  late ITodoService _todoService;

  var todos = [].obs;
  var _isLoading = true.obs;

  get isLoading => _isLoading.value;

  @override
  void onInit() {
    super.onInit();
    _todoService = TodoService();
    this.getTotos();
  }

  Future<void> getTotos() async {
    try {
      final result = await _todoService.getTodos();
      this.todos.assignAll(result);
      _isLoading(false);
    } catch (e) {
      print(e);
      _isLoading(false);
    } finally {
      _isLoading(false);
    }
  }
}
